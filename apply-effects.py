#!/usr/bin/env python2

################################################################################
## Apply some effects to a video file and save the output.
## Copyright (C) 2014  Rachel Domagalski: rsdomagalski@gmail.com
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## ## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

import cv2
import sys
import argparse
import numpy as np
import subprocess as spr
import matplotlib as mpl; mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.animation as anim
from cv2 import cv

def exp_smooth(smooth, current, previous):
    """
    Exponential smoothing for 8-bit data.
    """
    frame = smooth * current.astype(float) + (1.0-smooth)*previous.astype(float)
    return frame.astype(np.uint8)

def get_dim(cap):
    """
    Get the dimentions of the input video.
    """
    width = cap.get(cv.CV_CAP_PROP_FRAME_WIDTH)
    height = cap.get(cv.CV_CAP_PROP_FRAME_HEIGHT)
    return (int(height), int(width))

def get_duration(fname):
    """
    Use ffpoble to get the duration in seconds of the input video.
    """
    subpr = spr.Popen(['ffprobe',fname], stdout=spr.PIPE, stderr=spr.PIPE)
    _, ffprobe = subpr.communicate()
    ffprobe = filter(lambda s: 'Duration:' in s, ffprobe.split('\n'))[-1]
    ffprobe = ffprobe[ffprobe.find(':')+2:ffprobe.find(',')].split(':')
    duration = sum([d*60**(2-i) for i, d in enumerate(map(float, ffprobe))])
    return duration

def print_progress(step, total, prog_str='Percent complete:'):
    """
    Print the progress of some iteration through data.
    """
    progress = round(100 * float(step) / total, 2)
    progress = '\r' + prog_str + ' ' + str(progress) + '%\t\t'
    print progress,
    if step == total:
        print
    else:
        sys.stdout.flush()

if __name__ == '__main__':
    # Parse arguments from the command line.
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--bitrate', type=int, default=5000,
                        help='Video bitrate.')
    parser.add_argument('-i', '--input', required=True,
                        help='Input file to read from.')
    parser.add_argument('-I', '--invert', action='store_true',
                        help='Bitwise inversion before data offset.')
    parser.add_argument('-F', '--FPS', type=float, default=30,
                        help='Set the number of frames per second.')
    parser.add_argument('-m', '--vmin', type=int, default=0,
                        help='Set minimum of the colorbar.')
    parser.add_argument('-M', '--vmax', type=int, default=255,
                        help='Set maximum of the colorbar.')
    parser.add_argument('-o', '--output',
                        required=True,
                        help='File to save the output to.')
    parser.add_argument('-O', '--offset', type=int, default=0,
                        help='Offset to subtract from all frames.')
    parser.add_argument('-s', '--smooth', type=float, default=0.1,
                        help='Exponential smoothing factor.')
    args = parser.parse_args()

    # Open the video input
    cap = cv2.VideoCapture(args.input)
    seconds = get_duration(args.input)

    # Set up the output writer.
    FFMpegWriter = anim.writers['ffmpeg']
    metadata = dict(title='Movie Test', artist='Matplotlib',
                    comment='Movie support!')
    writer = FFMpegWriter(fps=args.FPS, metadata=metadata, bitrate=args.bitrate)

    # Set up the frame template.
    prev = np.zeros(get_dim(cap), dtype=np.uint8)
    fig = plt.figure()
    img = plt.imshow(prev, vmin=args.vmin, vmax=args.vmax)
    plt.subplots_adjust(0, 0, 1, 1)
    plt.axis('off')

    # Loop over all frames and save the new frames to a video.
    with writer.saving(fig, args.output, 100):
        nframe = 0
        while(cap.isOpened()):
            nframe += 1
            ret, frame = cap.read()
            if not ret:
                break

            # Convert data to grayscale, then get a background via smoothing.
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            bgnd = exp_smooth(0.1, gray, prev)
            prev = bgnd[:]

            # TODO need a good way of skipping frames.
            #if nframe % 25:
            #    continue

            # Set the data
            if args.invert:
                img.set_data(~(gray - bgnd) - args.offset)
            else:
                img.set_data(gray - bgnd - args.offset)
            writer.grab_frame()
            print_progress(cap.get(cv.CV_CAP_PROP_POS_MSEC)/1000, seconds)

        print
        cap.release()
        cv2.destroyAllWindows()
